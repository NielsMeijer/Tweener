﻿using UnityEngine;

namespace NMeijer.Tweening
{
	public class CanvasGroupTweenerTrack : TweenerTrack
	{
		#region Editor Variables

		[SerializeField]
		private CanvasGroup _canvasGroup = default;
		[SerializeField]
		private float _fromAlpha = default;
		[SerializeField]
		private float _toAlpha = default;

		#endregion

		#region Public Methods

		public override void SetEnd()
		{
			_toAlpha = _canvasGroup.alpha;
		}

		public override void SetStart()
		{
			_fromAlpha = _canvasGroup.alpha;
		}

		#endregion

		#region Protected Methods

		protected override void Lerp(float progress)
		{
			if(_canvasGroup != null)
			{
				_canvasGroup.alpha = Mathf.LerpUnclamped(_fromAlpha, _toAlpha, progress);
			}
		}

		#endregion
	}
}
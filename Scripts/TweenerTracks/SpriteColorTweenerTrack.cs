﻿using UnityEngine;

namespace NMeijer.Tweening
{
	public class SpriteColorTweenerTrack : TweenerTrack
	{
		#region Editor Variables

		[SerializeField]
		private SpriteRenderer _spriteRenderer = default;

		[SerializeField]
		private Color _startColor = default;
		[SerializeField]
		private Color _endColor = default;

		#endregion

		#region Public Methods

		public override void SetStart()
		{
			_startColor = _spriteRenderer.color;
		}

		public override void SetEnd()
		{
			_endColor = _spriteRenderer.color;
		}

		#endregion

		#region Protected Methods

		protected override void Lerp(float progress)
		{
			_spriteRenderer.color = Color.Lerp(_startColor, _endColor, progress);
		}

		#endregion
	}
}
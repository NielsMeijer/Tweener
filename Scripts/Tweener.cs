﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("NMeijer.Tweener.Editor")]
namespace NMeijer.Tweening
{
	public class Tweener : MonoBehaviour
	{
		#region Variables

		[SerializeField]
		private TweenerTrack[] _tweenerTracks = new TweenerTrack[0];
		[SerializeField]
		private bool _allowPlayingWhileBusy = default;
		[SerializeField]
		private bool _playOnAwake = default;
		[SerializeField]
		private bool _loop = default;
		[SerializeField]
		private bool _loopBackAndForth = default;

		#endregion

		#region Properties

		public float EndTime
		{
			get; private set;
		}

		public bool IsPlaying
		{
			get; private set;
		}

		public TweenerTrack[] TweenerTracks => _tweenerTracks;

		#endregion

		#region Variables

		private Coroutine _waitForEndRoutine;

		private Action _callback;

		#endregion

		#region Lifecycle

		private void Awake()
		{
			Initialize();
		}

		#endregion

		#region Public Methods

		// Allows for UnityEvents to be used
		public void Play()
		{
			Play(null);
		}

		public void Play(Action callback)
		{
			if(!IsPlaying || _allowPlayingWhileBusy)
			{
				StartWaitForEnd(true);
				for(int i = 0, length = _tweenerTracks.Length; i < length; i++)
				{
					_tweenerTracks[i].Play();
				}

				_callback = callback;
			}
		}

		// Allows for UnityEvents to be used
		public void PlayReverse()
		{
			PlayReverse(null);
		}

		public void PlayReverse(Action callback)
		{
			if(!IsPlaying || _allowPlayingWhileBusy)
			{
				StartWaitForEnd(false);
				for(int i = 0, length = _tweenerTracks.Length; i < length; i++)
				{
					_tweenerTracks[i].Play(true);
				}

				_callback = callback;
			}
		}

		public void Stop()
		{
			if(IsPlaying)
			{
				if(_waitForEndRoutine != null)
				{
					StopCoroutine(_waitForEndRoutine);
				}

				IsPlaying = false;

				for(int i = 0, length = _tweenerTracks.Length; i < length; i++)
				{
					_tweenerTracks[i].Stop();
				}
			}
		}

		public void Show(float progress)
		{
			for(int i = 0, length = _tweenerTracks.Length; i < length; i++)
			{
				_tweenerTracks[i].Show(progress);
			}
		}

		#endregion

		#region Private Methods

		private void Initialize()
		{
			EndTime = 0;
			int length = _tweenerTracks.Length;
			for(int i = 0; i < length; i++)
			{
				TweenerTrack track = _tweenerTracks[i];
				if(track.EndTime > EndTime)
				{
					EndTime = track.EndTime;
				}
			}
			for(int i = 0; i < length; i++)
			{
				_tweenerTracks[i].Initialize(EndTime);
			}

			if(_playOnAwake && Application.isPlaying)
			{
				Play();
			}
		}

		private void StartWaitForEnd(bool playedForward = true)
		{
			if(_waitForEndRoutine != null)
			{
				StopCoroutine(_waitForEndRoutine);
			}
			_waitForEndRoutine = StartCoroutine(WaitForEnd(playedForward));
			IsPlaying = true;
		}

		private IEnumerator WaitForEnd(bool playedForward = true)
		{
			yield return new WaitForSeconds(EndTime);
			IsPlaying = false;

			if(_loop)
			{
				Play();
			}
			else if(_loopBackAndForth)
			{
				if(playedForward)
				{
					PlayReverse();
				}
				else
				{
					Play();
				}
			}

			_waitForEndRoutine = null;
			_callback?.Invoke();
		}

		#endregion

		#region Editor

#if UNITY_EDITOR
		internal TweenerTrack[] EditorTweenerTracks => _tweenerTracks;

		internal void EditorInitialize()
		{
			Initialize();
		}
#endif

		#endregion
	}
}